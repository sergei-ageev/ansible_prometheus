#!/bin/bash
VAULT_PASSWORD=''
SERVER_IP=''
BECOME_PASSWORD=''
REMOTE_USER=''

PROMETHEUS_VERSION='2.37.6'
$NODE_EXPORTER_VERSION='1.5.0'

echo "$VAULT_PASSWORD" > VAULT_PASS.txt

sed -i "s/<свой_ip_адрес_сервера_prometheus>/$SERVER_IP/" ./hosts
sed -i "s/<свой_ip_адрес_node_exporter>/$SERVER_IP/" ./hosts

sed -i "s/<имя_пользователя>/$REMOTE_USER/" ./host_vars/prometheus-1/var.yaml
sed -i "s/<имя_пользователя>/$REMOTE_USER/" ./host_vars/node_exporter-1/var.yaml

ansible-vault encrypt_string --vault-password-file VAULT_PASS.txt --name 'ansible_become_pass' "$BECOME_PASSWORD" \
                             >> ./host_vars/prometheus-1/var.yaml
ansible-vault encrypt_string --vault-password-file VAULT_PASS.txt --name 'ansible_become_pass' "$BECOME_PASSWORD" \
                             >> ./host_vars/node_exporter-1/var.yaml

sed -i "s/2.37.6/$PROMETHEUS_VERSION/" ./roles/install_prometheus/defaults/main.yaml
sed -i "s/1.5.0/$NODE_EXPORTER_VERSION/" ./roles/install_node_exporter/defaults/main.yaml
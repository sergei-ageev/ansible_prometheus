# Описание
В данном репозитории представлены самописные роли для Ansible по установке:
- Prometheus
- Node Exporter

Действия, указанные ниже, необходимо производить на АРМ с установленным Ansible.
Также предполагается, что между АРМ и машинами, где будет установлен Prometheus и Node Exporter, уже
налажена связь по SSH с использованием публичного и приватного RSA ключей.

Установка производится на машины с ОС Ubuntu.

# Подготовка файлов для установки Prometheus
1.  Переходим в домашнюю директорию пользователя и копируем репозиторий:
```  
cd ~
git clone https://gitlab.com/sergei-ageev/ansible_prometheus
```
2. Переходим в папку со скаченным репозитоием. Вставляем путь к своему приватному SSH ключу
в файл ansible.cfg в переменную private_key_file.
```
cd ~/ansible_prometheus
sed -i "s/<имя_пользователя>/$USER/" ./ansible.cfg
```
Если используется другой тип ключей (отличный от RSA), то необходимо отредактировать private_key_file
в файле ansible.cfg вручную.

3. Делаем скрипт config.sh исполняемым.
```
chmod +x config.sh
```

4. Заполняем переменные в скрипте config.sh:
```
VAULT_PASSWORD - - пароль для расшифровки vault ansible.
SERVER_IP - ip адрес сервера, куда устанавливается Prometheus и Node Exporter.
BECOME_PASSWORD - ip адрес удаленного пользователя для выполнения команд через sudo.
REMOTE_USER - имя удаленного пользователя, от которого производится установка.

Также Вы можете изменить устанавливаемые версии программ.
```

5. Запускаем скрипт, который заполнит недостающие данные, необходимые для установки.
```
./config.sh
```

6. Опционально редактируем конфигурационный файл prometheus.service, находящийся тут
./roles/install_prometheus/files/prometheus.service.j2

7. Опционально редактируем конфигурационный файл node_exporter.service, находящийся тут
./roles/install_node_exporter/files/node_exporter.service.j2

8. Запускаем установку командой:
```
ansible-playbook playbook.yaml --vault-password-file VAULT_PASS.txt
```

9. Проверяем работоспобность путем перехода на страницу http://<ip_адрес_сервера_prometheus>:9090/targets